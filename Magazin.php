<?php

include 'Televizor.php';

class Magazin
{

    private $televizors;

    public function __construct()
    {
        $this->televizors = array(
            new Televizor('LG', 'Android', 40, 1000),
            new Televizor('LG', 'Android', 45, 1500),
            new Televizor('Sony', 'WebOS', 45, 1500),
            new Televizor('Philips', 'Tizer', 50, 2000),
            new Televizor('Philips', 'Tizer', 50, 3500),

        );
    }

    public function getTelevizorsCountBySystem($system){
        $count = 0;

        foreach ($this->televizors as $televizor){
            if($televizor->getSystem() == $system){
                $count++;
            }
        }
        return $count;
    }

    public function getTelevezorsWithDiagonal($diagonal_count){
        $televizors = array();

        foreach ($this->televizors as $televizor){
            if ($televizor->getDiagonal() >= $diagonal_count){
                $televizors[] = $televizor;
            }
            return $televizors;
        }
    }





}