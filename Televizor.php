<?php


class Televizor
{
    private $model;
    private $system;
    private $diagonal;
    private $cost;

    /**
     * Televizor constructor.
     * @param $model
     * @param $system
     * @param $diagonal
     * @param $cost
     */
    public function __construct($model, $system, $diagonal, $cost)
    {
        $this->model = $model;
        $this->system = $system;
        $this->diagonal = $diagonal;
        $this->cost = $cost;
    }

    /**
     * @return mixed
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * @param mixed $model
     */
    public function setModel($model)
    {
        $this->model = $model;
    }

    /**
     * @return mixed
     */
    public function getSystem()
    {
        return $this->system;
    }

    /**
     * @param mixed $system
     */
    public function setSystem($system)
    {
        $this->system = $system;
    }

    /**
     * @return mixed
     */
    public function getDiagonal()
    {
        return $this->diagonal;
    }

    /**
     * @param mixed $diagonal
     */
    public function setDiagonal($diagonal)
    {
        $this->diagonal = $diagonal;
    }

    /**
     * @return mixed
     */
    public function getCost()
    {
        return $this->cost;
    }

    /**
     * @param mixed $cost
     */
    public function setCost($cost)
    {
        $this->cost = $cost;
    }


}