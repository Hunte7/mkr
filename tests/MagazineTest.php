<?php
include 'Magazin.php';

class MagazineTest extends \PHPUnit\Framework\TestCase
{

    private $magazine;

    protected function setUp(): void
    {
      $this->magazine = new Magazin();
    }

    public function testTelevizor()
    {
        $televizors = $this->magazine->getTelevizorsCountBySystem(4);

        $this->assertEquals(4, $televizors);
    }


    /**
     * @dataProvider addDataProvider
     * @param $diagonal
     */
    public function testDiagonalCount($diagonal)
    {
        $result = $this->magazine->getTelevezorsWithDiagonal($diagonal);
        $this->assertEquals( 2, $diagonal);
    }


    public function addDataProvider() {
        return array(
            array(2,45),
            array(2,50),
        );
    }
    protected function tearDown() : void
    {
        isset($this->magazine);
    }

}